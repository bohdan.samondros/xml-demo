package org.example.sax;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

public class ValidationDemo {
    public static void main(String[] args) throws Exception {
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(new File("employee.xsd"));
        SAXParserFactory saxFactory = SAXParserFactory.newInstance();
        saxFactory.setSchema(schema);
        SAXParser parser = saxFactory.newSAXParser();
        parser.parse("employee.xml", new DefaultHandler() {
            // TODO: other handler methods
            @Override
            public void error(SAXParseException e) throws SAXException {
                throw e;
            }
        });
    }
}
