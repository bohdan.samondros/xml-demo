package org.example.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class UnmarshalDemo {
    public static void main(String[] args) throws Exception {
        JAXBContext context = JAXBContext.newInstance(Employees.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        Employees employees = (Employees) unmarshaller.unmarshal(new File("employees.xml"));

        System.out.println(employees);
    }
}
