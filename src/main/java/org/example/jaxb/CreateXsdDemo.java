package org.example.jaxb;

import javax.xml.bind.JAXBContext;

public class CreateXsdDemo {
    public static void main(String[] args) throws Exception {
        JAXBContext context = JAXBContext.newInstance(Employees.class);
        context.generateSchema(new MySchemaOutputResolver("employee2.xsd"));
    }
}
