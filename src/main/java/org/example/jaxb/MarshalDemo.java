package org.example.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.Date;
import java.util.List;

public class MarshalDemo {
    public static void main(String[] args)  throws Exception {
        JAXBContext context = JAXBContext.newInstance(Employees.class);
        Employees employees = new Employees();
        org.example.jaxb.Employee employee = new org.example.jaxb.Employee();
        employee.setId(1);
        employee.setName("Vasya");
        employee.setDateOfBirth(new Date());
        employee.setRole(List.of("Role1", "Role2"));

        employees.getEmployees().add(employee);

//        Unmarshaller unmarshaller = context.createUnmarshaller();
        Marshaller marshaller = context.createMarshaller();

        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(employees, new File("employee.xml"));
        marshaller.marshal(employees, System.out);
    }
}
