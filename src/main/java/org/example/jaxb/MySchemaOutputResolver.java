package org.example.jaxb;

import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

public class MySchemaOutputResolver extends SchemaOutputResolver {

    private String fileName;

    public MySchemaOutputResolver(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Result createOutput(String namespaceURI, String suggestedFileName) throws IOException {
        File file = new File(fileName);
        StreamResult result = new StreamResult(file);
        result.setSystemId(file.toURI().toURL().toString());
        return result;
    }
}