package org.example.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

import static org.w3c.dom.traversal.NodeFilter.*;

public class NodeIteratorDemo {
    public static void main(String[] args) throws Exception {
        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(new File("employees.xml"));
        document.getDocumentElement().normalize();

        DocumentTraversal documentTraversal = (DocumentTraversal) document;

        NodeIterator nodeIterator = documentTraversal.createNodeIterator(document, SHOW_ELEMENT, new MyFilter(), true);

        Node node;

        while ((node = nodeIterator.nextNode()) != null) {
            System.out.println(node.getNodeName());
            System.out.println(node.getTextContent());
        }
    }

    public static class MyFilter implements NodeFilter {

        @Override
        public short acceptNode(Node thisNode) {
            if (thisNode.getNodeType() == Node.ELEMENT_NODE) {

                Element e = (Element) thisNode;
                String nodeName = e.getNodeName();

                if ("age".equals(nodeName) || "role".equals(nodeName)) {
                    return NodeFilter.FILTER_ACCEPT;
                }
            }

            return NodeFilter.FILTER_REJECT;
        }
    }
}
