package org.example.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeIterator;
import org.w3c.dom.traversal.TreeWalker;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

import static org.w3c.dom.traversal.NodeFilter.SHOW_ALL;
import static org.w3c.dom.traversal.NodeFilter.SHOW_ELEMENT;

public class TreeWalkerDemo {
    public static void main(String[] args) throws Exception {
        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(new File("employees.xml"));
        document.getDocumentElement().normalize();

        DocumentTraversal documentTraversal = (DocumentTraversal) document;

        TreeWalker treeWalker = documentTraversal.createTreeWalker(document, SHOW_ELEMENT, null, true);

        Node node;

        while ((node = treeWalker.lastChild()) != null) {
            System.out.println(node.getNodeName());
        }
    }
}
