package org.example.dom;

import org.example.Employee;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class Main {
    public static void main(String[] args) throws Exception {
        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(new File("employees.xml"));
        document.getDocumentElement().normalize();

        Element root = document.getDocumentElement();
        NodeList childNodes = root.getElementsByTagName("Employee");

        for (int i = 0; i < childNodes.getLength(); i++) {
            Employee employee = new Employee();
            Element employeeNode = (Element) childNodes.item(i);

            String id = employeeNode.getAttribute("id");
            employee.setId(Integer.parseInt(id));

            String age = employeeNode.getElementsByTagName("age").item(0).getTextContent();
            employee.setAge(Integer.parseInt(age));

            String name = employeeNode.getElementsByTagName("name").item(0).getTextContent();
            employee.setName(name);

            String gender = employeeNode.getElementsByTagName("gender").item(0).getTextContent();
            employee.setGender(gender);

            String role = employeeNode.getElementsByTagName("role").item(0).getTextContent();
            employee.setRole(role);

            System.out.println(employee);
        }
    }
}
