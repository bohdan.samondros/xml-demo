package org.example.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class DomWriteDemo {
    public static void main(String[] args) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        Element root = doc.createElementNS("namespace1.xsd", "Employees");
        doc.appendChild(root);

        root.appendChild(createEmployee(doc, 1, "Robert", 25, "programmer"));
        root.appendChild(createEmployee(doc, 2, "Pamela", 26, "writer"));
        root.appendChild(createEmployee(doc, 3, "Peter", 27, "teacher"));

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transf = transformerFactory.newTransformer();

        transf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transf.setOutputProperty(OutputKeys.INDENT, "yes");
        transf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        DOMSource source = new DOMSource(doc);

        File myFile = new File("employees_write.xml");

        StreamResult console = new StreamResult(System.out);
        StreamResult file = new StreamResult(myFile);

        transf.transform(source, console);
        transf.transform(source, file);
    }

    private static Node createEmployee(Document doc, int id, String name, int age, String role) {
        Element employee = doc.createElement("Employee");

        employee.setAttribute("id", String.valueOf(id));
        employee.appendChild(createEmployeeElement(doc, "name", name));
        employee.appendChild(createEmployeeElement(doc, "age", String.valueOf(age)));
        employee.appendChild(createEmployeeElement(doc, "role", role));

        return employee;
    }

    private static Node createEmployeeElement(Document doc, String name,
                                          String value) {

        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));

        return node;
    }
}
